// 1

// class Triangle {
//     #leftSide;
//     #rightSide;
//     #width;
  
//     constructor() {
//       this.#leftSide = 0;
//       this.#rightSide = 0;
//       this.#width = 0;
//     }
  
//     getLeftSide() {
//       return this.#leftSide;
//     }
  
//     getRightSide() {
//       return this.#rightSide;
//     }
  
//     getWidth() {
//       return this.#width;
//     }
  
//     setSides(a, b, c) {
//       this.#leftSide = a;
//       this.#rightSide = b;
//       this.#width = c;
//     }
//   }
// const triangle = new Triangle();
// triangle.setSides(3,4,5);

// console.log(triangle.getLeftSide());
// console.log(triangle.getRightSide());
// console.log(triangle.getWidth());



// 2

// class Employee{
//     #id
//     #firstName
//     #lastName
//     #salary

//     constructor(id, firstName, lastName, salary) {
//         this.#id = id;
//         this.#firstName = firstName;
//         this.#lastName = lastName;
//         this.#salary = salary;
//     }

//     getId() {
//         return this.#id;
//     }
    
//     getFirstName() {
//         return this.#firstName;
//     }
    
//     getLastName() {
//         return this.#lastName;
//     }
//     getSalary() {
//         return this.#salary;
//     }

//     gId(id) {
//         this.#id = id;
//       }
    
//     gFirstName(firstName) {
//         this.#firstName = firstName;
//     }
    
//     gLastName(lastName) {
//         this.#lastName = lastName;
//     }
    
//     gSalary(salary) {
//         this.#salary = salary;
//     }
    
//     getFullName() {
//         return `${this.#firstName} ${this.#lastName}`;
//     }
    
//     getAnnualSalary() {
//         return this.#salary * 12;
//     }
    
//     raiseSalary(percent) {
//         const raiseAmount = this.#salary * (percent / 100);
//         this.#salary += raiseAmount;
//     }
// }
// const employee = new Employee(1, "John", "Doe", 5000);

// console.log(employee.getId());
// console.log(employee.getFirstName());
// console.log(employee.getLastName());
// console.log(employee.getSalary());

// employee.gLastName("Smith");
// employee.gSalary(6000);

// console.log(employee.getFullName());
// console.log(employee.getAnnualSalary());

// employee.raiseSalary(10);

// console.log(employee.getSalary());



// 3

// class CustomDate {
//     #day
//     #month
//     #year

//     constructor(day, month, year) {
//       this.#day = day;
//       this.#month = month;
//       this.#year = year;
//     }

//     getDay() {
//         return this.#day;
//     }
    
//     getMonth() {
//         return this.#month;
//     }
    
//     getYear() {
//         return this.#year;
//     }

//     sDay(day) {
//         this.#day = day;
//     }
    
//     sMonth(month) {
//         this.#month = month;
//     }
    
//     sYear(year) {
//         this.#year = year;
//     }

//     getISOdate() {
//         const formattedDay = this.#day.toString().padStart(2, "0");
//         const formattedMonth = this.#month.toString().padStart(2, "0");
//         return `${formattedDay}-${formattedMonth}-${this.#year}`;
//     }
// }
// const customDate = new CustomDate(12, 5, 2023);

// console.log(customDate.getDay());
// console.log(customDate.getMonth());
// console.log(customDate.getYear());

// customDate.sDay(25);
// customDate.sMonth(10);
// customDate.sYear(2023);

// console.log(customDate.getISOdate());


// 4

// class Time {
//     #hour
//     #minute
//     #second

//     constructor(hour, minute, second) {
//       this.#hour = hour;
//       this.#minute = minute;
//       this.#second = second;
//     }

//     getHour() {
//         return this.#hour;
//     }
    
//     getMinut() {
//         return this.#minute;
//     }
    
//     getSecond() {
//         return this.#second;
//     }

   
//   setHour(hour) {
//     this.#hour = hour;
//   }

//   setMinute(minute) {
//     this.#minute = minute;
//   }

//   setSecond(second) {
//     this.#second = second;
//   }

//   nextSecond() {
//     this.#second++;
//     if (this.#second === 60) {
//       this.#second = 0;
//       this.#minute++;
//       if (this.#minute === 60) {
//         this.#minute = 0;
//         this.#hour++;
//         if (this.#hour === 24) {
//           this.#hour = 0;
//         }
//       }
//     }
//   }

//   previousSecond() {
//     this.#second--;
//     if (this.#second === -1) {
//       this.#second = 59;
//       this.#minute--;
//       if (this.#minute === -1) {
//         this.#minute = 59;
//         this.#hour--;
//         if (this.#hour === -1) {
//           this.#hour = 23;
//         }
//       }
//     }
//   }
// }

// const time = new Time(12, 30, 45);

// console.log(time.getHour());
// console.log(time.getMinut());
// console.log(time.getSecond());

// time.nextSecond();

// console.log("Ikkinchi");

// console.log(time.getHour());
// console.log(time.getMinut());
// console.log(time.getSecond());

// console.log("Oldingi ikki");
// time.previousSecond();

// console.log(time.getHour());
// console.log(time.getMinut());
// console.log(time.getSecond());




// 5-Misol,   SHAPE, CIRCLE,  RECTANGLE   Inheritance

// class Shape {
//     #color;
//     #filled;
  
//     constructor(color, filled) {
//       this.#color = color;
//       this.#filled = filled;
//     }
  
//     getColor() {
//       return this.#color;
//     }
  
//     setColor(color) {
//       this.#color = color;
//     }
  
//     isFilled() {
//       return this.#filled;
//     }
  
//     setFilled(filled) {
//       this.#filled = filled;
//     }
// }
// class Circle extends Shape {
//     #radius;
  
//     constructor(color, filled, radius) {
//       super(color, filled);
//       this.#radius = radius;
//     }
  
//     getRadius() {
//       return this.#radius;
//     }
// }
// class Rectangle extends Shape {
//     #width;
//     #height;
  
//     constructor(color, filled, width, height) {
//       super(color, filled);
//       this.#width = width;
//       this.#height = height;
//     }
  
//     getWidth() {
//       return this.#width;
//     }
  
//     getHeight() {
//       return this.#height;
//     }
// }
// const circle = new Circle("red", true, 5);
// const rectangle = new Rectangle("blue", false, 4, 6);  

// console.log("Circle color:", circle.getColor());
// console.log("Is Circle filled?", circle.isFilled());
// console.log("Circle radius:", circle.getRadius());
  
// console.log("Rectangle color:", rectangle.getColor());
// console.log("Is Rectangle filled?", rectangle.isFilled());
// console.log("Rectangle width:", rectangle.getWidth());
// console.log("Rectangle height:", rectangle.getHeight());
  





// 6-Misol,     PERSON, STUDENT, EMPLOYE

// Person
// class Person {
//     #name;
//     #address;
  
//     constructor(name, address) {
//       this.#name = name;
//       this.#address = address;
//     }
  
//     getName() {
//       return this.#name;
//     }
  
//     setName(name) {
//       this.#name = name;
//     }
  
//     getAddress() {
//       return this.#address;
//     }
  
//     setAddress(address) {
//       this.#address = address;
//     }
// }
// const person = new Person("John Doe", "123 Main Street");
// console.log(person.getName());     
// console.log(person.getAddress());  

// person.setName("Jane Smith");
// person.setAddress("456 Elm Street");

// console.log(person.getName());     
// console.log(person.getAddress());   

// class Student extends Person {
//     #faculty;
//     #year;
//     #university;
  
//     constructor(name, address, faculty, year, university) {
//       super(name, address);
//       this.#faculty = faculty;
//       this.#year = year;
//       this.#university = university;
//     }
  
//     getFaculty() {
//       return this.#faculty;
//     }
  
//     getYear() {
//       return this.#year;
//     }
  
//     getUniversity() {
//       return this.#university;
//     }
  
//     setFaculty(faculty) {
//       this.#faculty = faculty;
//     }
  
//     setYear(year) {
//       this.#year = year;
//     }
  
//     setUniversity(university) {
//       this.#university = university;
//     }
// }
// const student = new Student("Alice Johnson", "123 Main Street", "Engineering", 2, "ABC University");
// console.log(student.getName());      
// console.log(student.getAddress());   
// console.log(student.getFaculty());   
// console.log(student.getYear());      
// console.log(student.getUniversity());

// student.setFaculty("Computer Science");
// student.setYear(3);
// student.setUniversity("XYZ University");
  
// console.log(student.getFaculty());   
// console.log(student.getYear());      
// console.log(student.getUniversity());

// class Employee extends Person {
//     #salary;
//     #work;
  
//     constructor(name, address, salary, work) {
//       super(name, address);
//       this.#salary = salary;
//       this.#work = work;
//     }
  
//     getSalary() {
//       return this.#salary;
//     }
  
//     getWork() {
//       return this.#work;
//     }
  
//     setSalary(salary) {
//       this.#salary = salary;
//     }
  
//     setWork(work) {
//       this.#work = work;
//     }
// }
// const employee = new Employee("John Smith", "456 Elm Street", 50000, "Software Engineer");
// console.log(employee.getName());      
// console.log(employee.getAddress());   
// console.log(employee.getSalary());    
// console.log(employee.getWork());     

// employee.setSalary(60000);
// employee.setWork("Senior Software Engineer");

// console.log(employee.getSalary());   
// console.log(employee.getWork());     
  




// 7-Misol   ANIMAL, MAMMAL, CAT, DOG, FISH, WHALE, SHARK

// class Animal {
//     constructor(name, speed, weight) {
//       this.name = name;
//       this.speed = speed;
//       this.weight = weight;
//     }
// }
// class Mammal extends Animal {
//     constructor(name, speed, weight) {
//       super(name, speed, weight);
//     }
// }
// class Fish extends Animal {
//     constructor(name, speed, weight) {
//       super(name, speed, weight);
//     }
// }
// class Cat extends Mammal {
//     constructor(name, speed, weight) {
//       super(name, speed, weight);
//     }
// }
// class Dog extends Mammal {
//     constructor(name, speed, weight) {
//       super(name, speed, weight);
//     }
// }
// class Whale extends Fish {
//     constructor(name, speed, weight) {
//       super(name, speed, weight);
//     }
// }
// class Shark extends Fish {
//     constructor(name, speed, weight) {
//       super(name, speed, weight);
//     }
// }
// const animal = new Animal("Animal", 10, 100);
// console.log(animal.name);    
// console.log(animal.speed);   
// console.log(animal.weight);  

// const cat = new Cat("Tom", 15, 5);
// console.log(cat.name);       
// console.log(cat.speed);      
// console.log(cat.weight);     

// const shark = new Shark("Jaws", 20, 500);
// console.log(shark.name);     
// console.log(shark.speed);    
// console.log(shark.weight);   

  



// 8- Misol  9-MIsol   Static keyword

// class Object {
//     static customKeys(obj) {
//       return Object.keys(obj);
//     }
  
//     static customValues(obj) {
//       return Object.values(obj);
//     }
  
//     static customEntries(obj) {
//       return Object.entries(obj);
//     }
// }
// class Number {
//     static customIsInteger(num) {
//       return Number.isInteger(num);
//     }
// }
// class Array {
//     static customIsArray(arr) {
//       return Array.isArray(arr);
//     }
// }
// class CustomArray {
//     #array;
  
//     constructor(array) {
//       if (Array.customIsArray(array) && array.every(Number.customIsInteger)) {
//         this.#array = array;
//       } else {
//         throw new Error('Invalid input. The array should contain only integer numbers.');
//       }
//     }
  
//     static isNumberArray(array) {
//       return Array.customIsArray(array) && array.every(Number.customIsInteger);
//     }
  
//     static sum(arr) {
//       return arr.reduce((total, num) => total + num, 0);
//     }
  
//     static max(arr) {
//       return Math.max(...arr);
//     }
  
//     static min(arr) {
//       return Math.min(...arr);
//     }
// }
// const validArray = [1, 2, 3, 4, 5];
// const invalidArray = [1, 2, 3, 4, '5'];

// console.log(CustomArray.isNumberArray(validArray));     
// console.log(CustomArray.isNumberArray(invalidArray));  

// const customArray = new CustomArray(validArray);

// console.log(CustomArray.sum(validArray));    
// console.log(CustomArray.max(validArray));    
// console.log(CustomArray.min(validArray));    




//  10-Misol   Polymorphism and inheritance

// class Person {
//     constructor(housePrice, housesNumber, carPrice, carsNumber, bankAccount) {
//       this.housePrice = housePrice;
//       this.housesNumber = housesNumber;
//       this.carPrice = carPrice;
//       this.carsNumber = carsNumber;
//       this.bankAccount = bankAccount;
//     }
  
//     getWealth() {
//       return (
//         this.housePrice * this.housesNumber +
//         this.carPrice * this.carsNumber +
//         this.bankAccount
//       );
//     }
// }
// class RichPerson extends Person {
//     constructor(
//       housePrice,
//       housesNumber,
//       carPrice,
//       carsNumber,
//       bankAccount,
//       companyPrice,
//       companiesNumber,
//       investigation
//     ) {
//       super(housePrice, housesNumber, carPrice, carsNumber, bankAccount);
//       this.companyPrice = companyPrice;
//       this.companiesNumber = companiesNumber;
//       this.investigation = investigation;
//     }
  
//     getWealth() {
//       return (
//         super.getWealth() +
//         this.companyPrice * this.companiesNumber +
//         this.investigation
//       );
//     }
// }
// const person = new Person(400000, 4, 70000, 5, 110000);
// console.log(person.getWealth()); 
// const richPerson = new RichPerson(
//   400000,
//   4,
//   70000,
//   5,
//   110000,
//   1100000,
//   5,
//   1100000
// );
// console.log(richPerson.getWealth()); 
